FROM php:7.3-apache

RUN a2enmod rewrite headers ssl

# install the PHP extensions we need
RUN set -ex \
	&& buildDeps=' \
		libjpeg62-turbo-dev \
    libzip-dev \
    libpng-dev \
		libpq-dev \
	' \
	&& apt-get update && apt-get install -y --no-install-recommends $buildDeps  \
	&& docker-php-ext-configure gd \
		--with-jpeg-dir=/usr \
		--with-png-dir=/usr \
	&& docker-php-ext-install -j "$(nproc)" mysqli gd mbstring opcache pdo pdo_mysql pdo_pgsql zip \
	&& apt-mark manual \
		libjpeg62-turbo \
		libpq5 \
	&& apt-get update


# set recommended PHP.ini settings
# see https://secure.php.net/manual/en/opcache.installation.php
RUN { \
		echo 'opcache.memory_consumption=128'; \
		echo 'opcache.interned_strings_buffer=8'; \
		echo 'opcache.max_accelerated_files=4000'; \
		echo 'opcache.revalidate_freq=60'; \
		echo 'opcache.fast_shutdown=1'; \
		echo 'opcache.enable_cli=1'; \
	} > /usr/local/etc/php/conf.d/opcache-recommended.ini

# Adjust php.ini Configs
RUN { \
		echo 'upload_max_filesize=100M'; \
	} > /usr/local/etc/php/conf.d/custom-configs.ini


COPY / /var/www/html

RUN cd /var/www/html \
    && chmod 755 -R /var/www/html/ \
    && chmod 644 /var/www/html/.htaccess \
    && chown www-data:www-data /var/www/html/ -R

# install Composer
RUN cd /tmp && curl -sS https://getcomposer.org/installer | php && mv composer.phar /usr/local/bin/composer

# install drush
RUN composer global require drush/drush:8.* \
	&& ln -s /root/.composer/vendor/bin/drush /usr/local/bin/drush


CMD /usr/sbin/apache2ctl -D FOREGROUND


