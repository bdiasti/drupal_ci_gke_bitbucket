<?php
$databases = [
  'default' =>
    [
      'default' =>
        [
          'database' => 'drupal_bd',
          'username' => getenv('DRUPAL_DB_USER'),
          'password' => getenv('DRUPAL_DB_PASSWORD'),
          'host' => '127.0.0.1',
          'port' => '3306',
          'driver' => 'mysql',
          'prefix' => '',
        ],
    ],

];
